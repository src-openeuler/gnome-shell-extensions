%global pkg_prefix gnome-shell-extension

Name:           gnome-shell-extensions
Version:        44.0
Release:        1
Summary:        Modify and extend GNOME Shell functionality and behavior
License:        GPLv2+
URL:            https://wiki.gnome.org/Projects/GnomeShell/Extensions
Source0:        https://download.gnome.org/sources/gnome-shell-extensions/44/%{name}-%{version}.tar.xz

BuildArch:      noarch

BuildRequires:  meson sassc
BuildRequires:  fdupes gnome-shell >= %{version} gobject-introspection
Requires:       gnome-shell >= %{version} nautilus gnome-menus

Provides:       %{pkg_prefix}-common = %{version}-%{release}  gnome-classic-session = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-common < %{version}-%{release}  gnome-classic-session < %{version}-%{release}
Provides:       %{pkg_prefix}-alternate-tab = %{version}-%{release}  %{pkg_prefix}-apps-menu = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-alternate-tab < %{version}-%{release}  %{pkg_prefix}-apps-menu < %{version}-%{release}
Provides:       %{pkg_prefix}-auto-move-windows = %{version}-%{release}  %{pkg_prefix}-drive-menu = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-auto-move-windows < %{version}-%{release}  %{pkg_prefix}-drive-menu < %{version}-%{release}
Provides:       %{pkg_prefix}-launch-new-instance = %{version}-%{release}  %{pkg_prefix}-native-window-placement = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-launch-new-instance < %{version}-%{release}  %{pkg_prefix}-native-window-placement < %{version}-%{release}
Provides:       %{pkg_prefix}-places-menu = %{version}-%{release}  %{pkg_prefix}-screenshot-window-sizer = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-places-menu < %{version}-%{release}  %{pkg_prefix}-screenshot-window-sizer < %{version}-%{release}
Provides:       %{pkg_prefix}-user-theme = %{version}-%{release}  %{pkg_prefix}-window-list = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-user-theme < %{version}-%{release}  %{pkg_prefix}-window-list < %{version}-%{release}
Provides:       %{pkg_prefix}-windowsNavigator = %{version}-%{release}  %{pkg_prefix}-workspace-indicator = %{version}-%{release}
Obsoletes:      %{pkg_prefix}-windowsNavigator < %{version}-%{release}  %{pkg_prefix}-workspace-indicator < %{version}-%{release}

%description
The GNOME Shell Extensions package contains a library of function which can support GNOME Shell
to execute additional and optional function.

%prep
%autosetup -n %{name}-%{version} -p1


%build
%meson \
    -D classic_mode=true \
    -D extension_set="all"
%meson_build

%install
%meson_install
%find_lang %{name} %{?no_lang_C}
%fdupes %{buildroot}%{_datadir}

mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
/bin/cp -v ./extensions/auto-move-windows/org.gnome.shell.extensions.auto-move-windows.gschema.xml \
            %{buildroot}/usr/share/glib-2.0/schemas/
/bin/cp -v ./extensions/native-window-placement/org.gnome.shell.extensions.native-window-placement.gschema.xml \
            %{buildroot}/usr/share/glib-2.0/schemas/
/bin/cp -v ./extensions/screenshot-window-sizer/org.gnome.shell.extensions.screenshot-window-sizer.gschema.xml \
            %{buildroot}/usr/share/glib-2.0/schemas/

mkdir -p %{buildroot}/usr/share/gnome-shell/extensions/screenshot-window-sizer
/bin/cp -v ./extensions/screenshot-window-sizer/* \
            %{buildroot}/usr/share/gnome-shell/extensions/screenshot-window-sizer

%files -f %{name}.lang
%doc NEWS README.md COPYING
%{_datadir}/gnome-shell/modes/classic.json
%{_datadir}/gnome-shell/theme/{*.svg,gnome-classic-high-contrast.css,gnome-classic.css}
%{_datadir}/xsessions/gnome-classic.desktop
%{_datadir}/xsessions/gnome-classic-xorg.desktop
%{_datadir}/glib-2.0/schemas/00_org.gnome.shell.extensions.classic.gschema.override
%{_datadir}/gnome-shell/extensions/{alternate-tab*/,apps-menu*/}
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.auto-move-windows.gschema.xml
%{_datadir}/gnome-shell/extensions/{auto-move-windows*/,drive-menu*/,launch-new-instance*/}
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.native-window-placement.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.screenshot-window-sizer.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.apps-menu.gschema.xml
%{_datadir}/gnome-shell/extensions/{native-window-placement*/,places-menu*/}
%{_datadir}/gnome-shell/extensions/screenshot-window-sizer*/
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.user-theme.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.shell.extensions.window-list.gschema.xml
%{_datadir}/gnome-shell/extensions/{user-theme*/,window-list*/,windowsNavigator*/,workspace-indicator*/}
%{_datadir}/wayland-sessions/gnome-*.desktop

%changelog
* Mon Nov 27 2023 lwg <liweiganga@uniontech.com> - 44.0-1
- update to version 44.0

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.1-1
- Update to 43.1

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-1
- Update to 42.2

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Update Version, Release, Source0, stage 'files'

* Tue Nov 26 2019 wangzhishun <wangzhishun1@huawei.com> - 3.30.1-2
- Package init
